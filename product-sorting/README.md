# Product Sorting API

Disclaimer: This document is used for Babyshop AB's technical interviews. It should not be used for any other purposes.

## Background

Product sorting system consists of API, a postgres store, and ML algorithm worker. __*The API is currently a missing part of the puzzel*__.

ML algorithm worker provides product sorting per customer to increase the webshop conversion rate. Overview of the system is presented in the following diagram:

![sorted product api](.resources/productSorting.png)

Every 1 min, the algo worker pushes the reranked products to postgres store.

## Getting started

### Tools needed

- Docker [download it](https://www.docker.com/products/docker-desktop)
- Docker Compose [read the doc](https://docs.docker.com/compose/install/)

### Start postgres and algoworker

After the tools installed, run

```
$ docker-compose up -d
```

Make sure you don't have any local application that is conflicting with the container postgres port 5432.

Two docker containers will be spinned up:
- ML Algo worker
- postgres.

Now you should be able to connect to postgres from container mirrored port to your host for example by, and Postgres login credential can be found in docker-compose.yaml file.

For example, if you have psql cli installed, you can connect to it by,

```
$ psql -h localhost -U postgres -d bsgproducts
```

### Database schema

![sorted product api](.resources/schema_diagram.png)

It contains product info data
- products, images table. one to many.

It contains user data
- user table

It contains ranking data
- product_ranking_per_users table

it contains a product user's favor state data, to save either the favor_state is `like` or `impassive` for an user and a product, lower case.
- user_product_favor_states


--------------------------

**Solution is only accepted in the following programming languages**

- javascript
- golang
- java

## Task 1

Compose an API to fetch sorted products for user, any of the following protocol implementation works, Restful/Graphql/Grpc.

### Requirements:

- Given a user ID, return a list of products
- products are sorted by their ranking
- API should present all product info that the postgres store contains. (No need to include date columns)
- No security needed.
- API's response should be meaningful



## Task 2
Add one more API endpoint, to mark a product' favor status either *like* or *impassive*.

### Requirements:

- Given a user ID, product ID and Favor status, when the endpoint is used, Favor status of the product is attached to the user.
- Input should be validated.
- No security needed.
- API's response should be meaningful


## Task 3

List the performance aspects of the product sorting system.

What is the performance pitfall of this system? How can it be improved?
